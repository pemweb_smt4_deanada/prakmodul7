<!-- PHP -->
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid;
        }
    </style>
</head>

<body>
    <!-- tambah proker -->
    <h2>Tambah Proker</h2>
    <form action="index.php" method="get">
        Nomor: <input type="text" name="nomorProker" id="nomorProker">
        Nama: <input type="text" name="namaProker" id="namaProker">
        SuratKeterangan: <input type="text" name="suratProker" id="suratProker">
        <input type="submit" value="Tambah" name="SubmitProker">
    </form>
    <!-- update proker -->
    <!-- <h2>Update Proker</h2>
    <form action="index.php" method="get">
        Nomor: <input type="text" name="nomorProker" id="nomorProker">
        Nama: <input type="text" name="namaProker" id="namaProker">
        SuratKeterangan: <input type="text" name="suratProker" id="suratProker">
        <input type="submit" value="Tambah" name="SubmitProker">
    </form> -->
    <!-- hapus proker -->
    <h2>Delete Proker</h2>
    <form action="index.php" method="get">
        Nama: <input type="text" name="namaProker" id="namaProker">
        <input type="submit" value="Delete" name="DeleteProker">
    </form>

    <h2>Daftar Program Kerja BEM</h2>

    <table>
        <thead>
            <tr>
                <td>No</td>
                <td>Nama Program Kerja</td>
                <td>Surat Keterangan</td>
                <td>Action 1</td>
                <td>Action 2</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proker[0] as $prokerKey): ?>
                <tr>
                    <td>
                        <?= $prokerKey["nomorProgram"] ?>
                    </td>
                    <td>
                        <?= $prokerKey["namaProgram"] ?>
                    </td>
                    <td>
                        <?= $prokerKey["suratKeterangan"] ?>
                    </td>
                    <td>Update</td>
                    <td>
                        <form action="index.php" method="get">
                            <input type="hidden" name="namaProker" value="<?= $prokerKey['namaProgram'] ?>">
                            <input type="submit" value="Delete" name="DeleteProker">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>