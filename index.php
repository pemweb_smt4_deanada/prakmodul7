<?php
include_once ("c_programKerja.php");
$controller = new c_programKerja();
if (isset($_GET["SubmitProker"])) {
    $nomor = $_GET["nomorProker"];
    $nama = $_GET["namaProker"];
    $surat = $_GET["suratProker"];
    $controller->createProker($nomor, $nama, $surat);
}
if (isset($_GET["DeleteProker"])) {
    $nama = $_GET["namaProker"];
    $controller->deleteProker($nama);
}
$controller->showProker();
?>